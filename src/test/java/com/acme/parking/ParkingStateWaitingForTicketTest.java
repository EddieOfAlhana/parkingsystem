package com.acme.parking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.mockito.Mockito;

import static org.testng.Assert.*;

public class ParkingStateWaitingForTicketTest {
	BufferedReader inputReader;
	PrintWriter outputWriter ;
	ParkingMeter parkingMeter ;
	
	@BeforeMethod
	public void setUp() {
		inputReader = Mockito.mock(BufferedReader.class);
		outputWriter = Mockito.mock(PrintWriter.class);
		parkingMeter = Mockito.spy(new ParkingMeter());
		parkingMeter.setInputReader(inputReader);
		parkingMeter.setOutputWriter(outputWriter);
	}
	
	@Test
	public void testDoTaskWithValidTicket() {
		//given
		try {
			Mockito.when(inputReader.readLine()).thenReturn("12345");
		} catch (IOException e) {
			e.printStackTrace();
		}
		ParkingStateWaitingForTicket sut = new ParkingStateWaitingForTicket(parkingMeter);
		
		
		//when
		ParkingMeterState newState = sut.doTask();
		
		//then
		assertTrue(newState instanceof ParkingStateAskForPayment);
	}
	
	@Test
	public void testDoTaskWithInvalidTicket() {
		//given
		try {
			Mockito.when(inputReader.readLine()).thenReturn("0");
		} catch (IOException e) {
			e.printStackTrace();
		}
		ParkingStateWaitingForTicket sut = new ParkingStateWaitingForTicket(parkingMeter);
		
		
		//when
		ParkingMeterState newState = sut.doTask();
		
		//then
		assertTrue(newState instanceof ParkingStateWaitingForTicket);
	}
}
