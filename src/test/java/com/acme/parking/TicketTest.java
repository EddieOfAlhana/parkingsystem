package com.acme.parking;

import junit.framework.Assert;

import org.testng.annotations.Test;

public class TicketTest {
	private Ticket sut;
	
  @Test
  public void testHoldsAnId() {
	  //given
	  sut = new Ticket(12345);
	  
	  //when
	  int result = sut.getId();
	  
	  //then
	  Assert.assertEquals(12345, result);
  }
  
  @Test
  public void testHoldsPrice() {
	  //given
	  sut = new Ticket(12345);
	  sut.setPrice(444);
	  
	  //when
	  int result = sut.getPrice();
	  
	  //then
	  Assert.assertEquals(444, result);
	  
  }
  
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testConstructorWithInvalidIdShouldThrowIllegalArgumentException() {
	  //given
	  sut = new Ticket(-1);
	  
	  //when
	  
	  //then
	  Assert.fail("Exception not thrown when constructing with invalid id");
  }
}
