package com.acme.parking;

import junit.framework.Assert;

import org.testng.annotations.Test;

public class GreatestFirstRefundStrategyTest {
	private GreatestFirstRefundStrategy sut;
	
	@Test(enabled = false)
	public void testRefund10ShouldReturn10() {
		//FIXME
		//given
		
		//when
		Money result = sut.getNextRefundableCoin(10);
		
		//then
		Assert.assertEquals(new Money(10), result);
	}
}
