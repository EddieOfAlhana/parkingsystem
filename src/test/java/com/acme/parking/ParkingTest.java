package com.acme.parking;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;

import org.mockito.Mockito;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;

public class ParkingTest {
	private ParkingMeter sut = new ParkingMeter();
	
	@Test
	public void testHoldsATicket() {
		//given
		Ticket ticket = Mockito.spy(new Ticket(12345));
		Mockito.when(ticket.getPrice()).thenReturn(200);
		
		//when
		sut.updateTicketPrice(ticket);
		
		//then
		assertEquals(200, ticket.getPrice());
	}
	
	@Test
	public void testPay() {
		//given
		Ticket ticket = new Ticket(123);
		
		//when
		sut.setTicketAndUpdateBalance(ticket);
		
		//then
		assertEquals(ticket, sut.getCurrentTicket());
	}
	
	public void testAcceptPayment() {
		//given
		Money money = new Money(100);
		
		//when
		int remainingBalance = sut.acceptPayment(money);
		
		//then
		assertEquals(100, remainingBalance);
	}
	
	public void testRefund100ShouldReturn100() {
		//given
		sut.setTicketAndUpdateBalance(new Ticket(0));
		sut.acceptPayment(new Money(100));
		
		List<Money> expected = new ArrayList<Money>();
		expected.add(new Money(100));
		
		
		//when
		List<Money> refundCoins = sut.refund();
		
		//then
		assertEqualsNoOrder(refundCoins.toArray(), expected.toArray());
	}
	
	@Test
	public void testRefund100ShouldReturn20() {
		//given
		sut.setTicketAndUpdateBalance(new Ticket(123));
		sut.acceptPayment(new Money(10));
		sut.acceptPayment(new Money(10));
		
		List<Money> expected = new ArrayList<Money>();
		expected.add(new Money(10));
		expected.add(new Money(10));
		
		
		//when
		List<Money> refundCoins = sut.refund();
		
		//then
		assertEqualsNoOrder(refundCoins.toArray(), expected.toArray());
	}
	
  @Test(dataProvider = "dp", enabled=false)
  public void f(Integer n, String s) {
  }
  

  @DataProvider
  public Object[][] dp() {
    return new Object[][] {
      new Object[] { 1, "a" },
      new Object[] { 2, "b" },
    };
  }
}
