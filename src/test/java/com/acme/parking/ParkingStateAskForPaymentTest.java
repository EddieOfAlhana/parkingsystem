package com.acme.parking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.mockito.Mockito;

import static org.testng.Assert.*;

public class ParkingStateAskForPaymentTest {
	BufferedReader inputReader;
	PrintWriter outputWriter ;
	ParkingMeter parkingMeter ;
	
	@BeforeMethod
	public void setUp() {
		inputReader = Mockito.mock(BufferedReader.class);
		outputWriter = Mockito.mock(PrintWriter.class);
		parkingMeter = Mockito.spy(new ParkingMeter());
		parkingMeter.setInputReader(inputReader);
		parkingMeter.setOutputWriter(outputWriter);
		Ticket ticket = new Ticket(12345);
		ticket.setPrice(200);
		Mockito.when(parkingMeter.getCurrentTicket()).thenReturn(ticket);
	}
	
	@Test
	public void testDoTaskWithFullAmountPaid() {
		//given
		try {
			Mockito.when(inputReader.readLine()).thenReturn("1000");
		} catch (IOException e) {
			e.printStackTrace();
		}
		ParkingStateAskForPayment sut = new ParkingStateAskForPayment(parkingMeter);
		
		
		//when
		ParkingMeterState newState = sut.doTask();
		
		//then
		assertTrue(newState instanceof ParkingStateRefundOverpayment, "Next state on partial payment is not 'Refund overpayment'");
	}
	
	@Test
	public void testDoTaskWithPartialAmountPaid() {
		//given
		try {
			Mockito.when(inputReader.readLine()).thenReturn("10");
		} catch (IOException e) {
			e.printStackTrace();
		}
		ParkingStateAskForPayment sut = new ParkingStateAskForPayment(parkingMeter);
		
		//when
		ParkingMeterState newState = sut.doTask();
		
		//then
		assertTrue(newState instanceof ParkingStateAskForPayment, "Next state on partial payment is not 'Ask for payment'");
	}
	
	@Test
	public void testDoTaskWithCancelledPayment() {
		//given
		try {
			Mockito.when(inputReader.readLine()).thenReturn("");
		} catch (IOException e) {
			e.printStackTrace();
		}
		ParkingStateAskForPayment sut = new ParkingStateAskForPayment(parkingMeter);		
		
		//when
		ParkingMeterState newState = sut.doTask();
		
		//then
		assertTrue(newState instanceof ParkingStateRefundDeposit, "Next state on partial payment is not 'Refund deposit'");
	}
}
