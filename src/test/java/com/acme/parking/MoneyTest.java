package com.acme.parking;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

public class MoneyTest {
  private Money sut;
  
  @Test
  public void testHoldsValue(){
	  //given
	  int value = 100;
	  sut = new Money( value );
	  //when
	  int result = sut.getValue();
	  //then
	  assertEquals(value, result);
  }
  
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testInvalidMoneyGeneration() {
	  //given
	  int value = 42;	  
	  //when
	  sut = new Money( value );
	  //then
	  fail("Excpetion not thrown on invalid money construction");
  }
}
