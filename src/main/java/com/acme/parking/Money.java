package com.acme.parking;

import java.util.Arrays;

public class Money {

	public static final Integer[] VALID_DENOMINATIONS = {10, 100, 1000};
	public static final int PRIME_FOR_HASHCODE = 31;
	
	private int denomination;
	
	public Money(int denomination) {
		if( !isValidDenomination(denomination) ) {
			throw new IllegalArgumentException("Invalid value for coinn construction: " + denomination);
		}
		this.denomination = denomination;
	}
	
	public int getValue() {
		return denomination;
	}
	
	private boolean isValidDenomination(int val ) {
		return Arrays.asList(VALID_DENOMINATIONS).contains(val);
	}

	@Override
	public int hashCode() {
		int result = 1;
		return PRIME_FOR_HASHCODE * result + denomination;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		Money other = (Money) obj;
		if (denomination != other.denomination) {
			return false;
		}
		return true;
	}
	
}
