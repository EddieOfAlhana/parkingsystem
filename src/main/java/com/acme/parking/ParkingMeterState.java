package com.acme.parking;

public interface ParkingMeterState {
	public ParkingMeterState doTask();
}
