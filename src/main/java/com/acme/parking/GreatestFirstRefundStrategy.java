package com.acme.parking;

import java.util.Arrays;

public class GreatestFirstRefundStrategy implements RefundStrategy {

	public Money getNextRefundableCoin(int sumToRefund) {
		Integer[] availableCoins = Money.VALID_DENOMINATIONS.clone();
		
		Arrays.sort(availableCoins);
		
		for(int i = availableCoins.length-1; i>=0; i--) {
			if (availableCoins[i] <= sumToRefund) {
				return new Money(availableCoins[i]);
			}
		}
		
		return null;
	}

}
