package com.acme.parking;

public class Ticket {
	private int id;
	private int price;
	

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Ticket(int ticketId) {
		if (ticketId <= 0) {
			throw new IllegalArgumentException("Ticket id should be gt 0");
		}
		id = ticketId;
	}

	public int getId() {
		return id;
	}

}
