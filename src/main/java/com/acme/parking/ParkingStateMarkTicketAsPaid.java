package com.acme.parking;

public class ParkingStateMarkTicketAsPaid implements ParkingMeterState {
	private ParkingMeter parkingMeter;
	
	public ParkingStateMarkTicketAsPaid(ParkingMeter parkingMeter) {
		this.parkingMeter = parkingMeter;
	}
	
	@Override
	public ParkingMeterState doTask() {
		
		return this;
	}
}
