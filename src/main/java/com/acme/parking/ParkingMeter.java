package com.acme.parking;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ParkingMeter {
	private Ticket currentTicket;
	private int currentBalance;
	private RefundStrategy refundStrategy;
	private ParkingMeterState currentState;
	private BufferedReader inputReader;
	private PrintWriter outputWriter;
	
	//TODO: Storing available money
	
	public ParkingMeter() {
		this(
			new GreatestFirstRefundStrategy(), 
			new BufferedReader(new InputStreamReader(System.in)), 
			new PrintWriter(System.out)
		);
	}
	
	public ParkingMeter(RefundStrategy refundStrategy, BufferedReader inputReader, PrintWriter outputWriter) {
		this.refundStrategy = refundStrategy;
		this.currentState = new ParkingStateWaitingForTicket(this);
	}

	public void updateTicketPrice(Ticket ticket) {
		//TODO: generating price logic
		ticket.setPrice(200);
	}

	public BufferedReader getInputReader() {
		return inputReader;
	}

	public void setInputReader(BufferedReader inputReader) {
		this.inputReader = inputReader;
	}

	public PrintWriter getOutputWriter() {
		return outputWriter;
	}

	public void setOutputWriter(PrintWriter outputWriter) {
		this.outputWriter = outputWriter;
	}

	public void setTicketAndUpdateBalance(Ticket ticket) {
		//TODO: save payed ticket id
		currentTicket = ticket;
		currentBalance = ticket.getPrice();
	}
	
	public Ticket getCurrentTicket() {
		return currentTicket;
	}

	public int acceptPayment(Money money) {
		//TODO: add money to available money 
		currentBalance -= money.getValue();
		return currentBalance;
	}
 
	public List<Money> refund() {
		List<Money> refundList =  new ArrayList<Money>();
		Money coinToRefund;
		
		while (currentBalance < 0) {
			refundList.add(coinToRefund = refundStrategy.getNextRefundableCoin(-currentBalance));
			currentBalance += coinToRefund.getValue(); 
		}
		return refundList;
	}	
	
	public void run() {
		while(true) {
			currentState = currentState.doTask();
			this.getOutputWriter().flush();
		}
	}
}
