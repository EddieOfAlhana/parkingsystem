package com.acme.parking;

public class ParkingStateRefundDeposit implements ParkingMeterState {
	private ParkingMeter parkingMeter;
	
	public ParkingStateRefundDeposit(ParkingMeter parkingMeter) {
		this.parkingMeter = parkingMeter;
	}
	
	@Override
	public ParkingMeterState doTask() {
		return this;
	}
}
