package com.acme.parking;

public class ParkingStateRefundOverpayment implements ParkingMeterState {
	private ParkingMeter parkingMeter;
	
	public ParkingStateRefundOverpayment(ParkingMeter parkingMeter) {
		this.parkingMeter = parkingMeter;
	}
	
	@Override
	public ParkingMeterState doTask() {
		return this;
	}
}
