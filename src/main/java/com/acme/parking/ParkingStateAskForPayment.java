package com.acme.parking;

import java.io.IOException;

public class ParkingStateAskForPayment implements ParkingMeterState {
	public static final String PAYMENT_HEADER = "\nYour Parking fee is: %d\nPlease start your payment!\nAcceptable currencies are: ";
	public static final String ASK_FOR_PAYMENT = "Please enter currency!";
	public static final String PAYMENT_ERROR_FORMAT = "%S is not an acceptable currency";
	public static final String PAYMENT_STATUS_FORMAT = "Paid: %d remaing fee: %d\n" + ASK_FOR_PAYMENT;
	public static final String SUCCESSFUL_PAYMENT = "Successfull payment! Thank you!";
	public static final String OVER_PAYMENT = "Your over payment is: %d";

	
	private ParkingMeter parkingMeter;
	
	public ParkingStateAskForPayment(ParkingMeter parkingMeter) {
		this.parkingMeter = parkingMeter;
	}
	
	@Override
	public ParkingMeterState doTask() {
		Ticket ticket = parkingMeter.getCurrentTicket();
		
		printPaymentHeader(ticket.getPrice());
		parkingMeter.setTicketAndUpdateBalance(ticket);
		String insertedCoin = "";
		try {
			insertedCoin = parkingMeter.getInputReader().readLine();
			if (null == insertedCoin || "".equals(insertedCoin)) {
				return new ParkingStateRefundDeposit(parkingMeter);
			}
			int balance = ticket.getPrice();
			int paid = 0;
			parkingMeter.getOutputWriter().println(ASK_FOR_PAYMENT);
		
			int currency = Integer.parseInt(insertedCoin);
			Money money = new Money(currency );
			paid += currency;
			balance = parkingMeter.acceptPayment(money);
			printPaymentStatus(paid, balance);
			if (balance <= 0) {
				return new ParkingStateRefundOverpayment(parkingMeter);
			} else {
				return this;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch( IllegalArgumentException ex ) {
			System.out.println( String.format(PAYMENT_ERROR_FORMAT, insertedCoin));
			return this;
		}
		return this;
	}
	
	private void printPaymentHeader(int fee) {
		parkingMeter.getOutputWriter().print( String.format(PAYMENT_HEADER, fee) );
		String separtor = "";
		for( Integer currency : Money.VALID_DENOMINATIONS ) {
			parkingMeter.getOutputWriter().print(separtor + currency.toString());
			separtor = " ";
		}
		parkingMeter.getOutputWriter().println();
	}
	
	private void printPaymentStatus(int paid, int balance) {
		if( balance > 0 ) {
			parkingMeter.getOutputWriter().println((String.format(PAYMENT_STATUS_FORMAT, paid, balance)));
		} else if( balance == 0 ){
			parkingMeter.getOutputWriter().println(SUCCESSFUL_PAYMENT);
		} else {
			parkingMeter.getOutputWriter().println(String.format(OVER_PAYMENT, -balance));
		}
	}
}
