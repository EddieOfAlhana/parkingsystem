package com.acme.parking;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class MainClass {
	
	public static void main(String[] args) {
		ParkingMeter parkingMeter = new ParkingMeter();
		parkingMeter.setInputReader(new BufferedReader(new InputStreamReader(System.in)));
		parkingMeter.setOutputWriter(new PrintWriter(System.out));
		parkingMeter.getOutputWriter().println("foo");
		
		parkingMeter.run();
	}
		
}
