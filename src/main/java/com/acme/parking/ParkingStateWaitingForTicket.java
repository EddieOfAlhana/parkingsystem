package com.acme.parking;

import java.io.IOException;

public class ParkingStateWaitingForTicket implements ParkingMeterState {
	public static final String ASK_FOR_TICKET_ID = "Please enter the ticket number: ";
	private ParkingMeter parkingMeter;
	
	public ParkingStateWaitingForTicket(ParkingMeter parkingMeter) {
		this.parkingMeter = parkingMeter;
	}
	
	@Override
	public ParkingMeterState doTask() {
		parkingMeter.getOutputWriter().println(ASK_FOR_TICKET_ID);
		
		int ticketId;
		
		try {
			ticketId = Integer.parseInt(parkingMeter.getInputReader().readLine());
			Ticket ticket = new Ticket(ticketId);
			parkingMeter.setTicketAndUpdateBalance(ticket);
			return new ParkingStateAskForPayment(parkingMeter);
			
		} catch (IllegalArgumentException e) {
			parkingMeter.getOutputWriter().println("Invalid ticket id!");
			return this;
		} catch (IOException e) {
			e.printStackTrace();
			return this;
		}
	}
}
