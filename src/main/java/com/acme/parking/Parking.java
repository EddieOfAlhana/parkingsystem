package com.acme.parking;

import java.util.ArrayList;
import java.util.List;

public class Parking {
	private Ticket currentTicket;
	private int currentBalance;
	private RefundStrategy refundStrategy;
	
	//TODO: Storing available money
	
	public Parking() {
		refundStrategy = new GreatestFirstRefundStrategy();
	}
	
	public Parking(RefundStrategy refundStrategy) {
		this.refundStrategy = refundStrategy;
	}

	public void updateTicketPrice(Ticket ticket) {
		//TODO: generating price logic
		ticket.setPrice(200);
	}

	public void pay(Ticket ticket) {
		//TODO: save payed ticket id
		currentTicket = ticket;
		currentBalance = ticket.getPrice();
	}
	
	public Ticket getCurrentTicket() {
		return currentTicket;
	}

	public int acceptPayment(Money money) {
		//TODO: add money to available money 
		currentBalance -= money.getValue();
		return currentBalance;
	}
 
	public List<Money> refund() {
		List<Money> refundList =  new ArrayList<Money>();
		Money coinToRefund;
		
		while (currentBalance < 0) {
			refundList.add(coinToRefund = refundStrategy.getNextRefundableCoin(-currentBalance));
			currentBalance += coinToRefund.getValue(); 
		}
		return refundList;
	}	
}
