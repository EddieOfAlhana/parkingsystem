package com.acme.parking;

public interface RefundStrategy {

	Money getNextRefundableCoin(int balance);
	
}
